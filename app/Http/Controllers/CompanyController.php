<?php

namespace App\Http\Controllers;

use Faker\Provider\ar_JO\Person;
use Illuminate\Http\Request;
use App\Company;
use App\Http\Resources\CompanyResource;
use App\Http\Resources\CompanyResourceCollection;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): CompanyResourceCollection
    {
        return new CompanyResourceCollection(Company::paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return CompanyResource
     */
    public function store(Request $request)
    {
        $request->validate([
            'company_name'      => 'required|max:50',
            'company_code'      => 'required|min:8|max:8',
            'company_address'   => 'required|max:100',
        ]);

        $company = Company::create($request->all());
        return new CompanyResource($company);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return CompanyResource
     * 
     */
    public function show(Company $company): CompanyResource
    {
        return new CompanyResource($company);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    public function update(Company $company, Request $request): CompanyResource
    {
        $request->validate([
            'company_name'      => 'required|max:50',
            'company_code'      => 'required|min:8|max:8',
            'company_address'   => 'required|max:100',
        ]);

        $company->update($request->all());

        return new CompanyResource($company);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $company->delete();
        return response()->json();
    }
}
