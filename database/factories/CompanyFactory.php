<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Company;
use Faker\Generator as Faker;
$factory->define(Company::class, function (Faker $faker) {
    return [
        'company_name' => $faker->company(),
        'company_code' => $faker->unique()->numberBetween(10000000, 10009999),
        'company_address' => $faker->address(),
    ];
});
